import abc
import logging
import pathlib
import random
import sys
import typing
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from scipy import optimize

X = 0
Y = 1


@dataclass
class DataWrapper:
    seed: int = 2020  # default seed value for random class
    data = None

    def __post_init__(self):
        self.random = random.Random(self.seed)
        self._k = 1000

    @property
    def k(self):
        return self._k

    def f(self, x):
        return 1.0 / (x ** 2.0 - 3.0 * x + 2.0)

    def prepare_data(self) -> 'DataWrapper':
        """
        Prepares the data for the algorithms
        """
        assert self.data is None

        def gen_data_point(ki):
            dk = self.random.normalvariate(mu=0, sigma=1)
            xk = 3.0 * ki / 1000.0
            fxk = self.f(xk)

            if fxk == np.nan:
                yk = np.nan
            elif fxk < -100.0:
                yk = -100.0 + dk
            elif fxk > 100.0:
                yk = 100.0 + dk
            else:
                yk = fxk + dk

            return xk, yk

        data = np.zeros((self.k + 1, 2))
        for k in range(0, self.k + 1):
            xk, yk = gen_data_point(k)
            data[k][X] = xk
            data[k][Y] = yk

        self.data = data
        return self


@dataclass
class Result:
    a: float
    b: float
    c: float
    d: float
    eval_count: int
    iter_count: int


class Task(abc.ABC):
    def __init__(self, data_wrapper: DataWrapper):
        self.eval_count = 0
        self.data_wrapper: DataWrapper = data_wrapper

    def approximant_function(self, x, a, b, c, d):
        return (
                ((a * x) + b)
                /
                (x ** 2 + c * x + d)
        )

    def means_of_least_squares(self, abcd):
        """
        Function to optimize. Calculates means of least squares
        """
        a, b, c, d = abcd
        assert self.data_wrapper is not None
        result = 0.0
        for k in range(0, self.data_wrapper.k + 1):
            x = self.data_wrapper.data[k][0]
            y = self.data_wrapper.data[k][1]
            evaluated_value = self.approximant_function(x, a, b, c, d)
            result += (evaluated_value - y) ** 2

        logging.debug("a, b, c, d -> %s = %s", abcd, result)
        self.eval_count += 1
        return result

    def _gen_y_clean(self, x, func: typing.Callable):
        y_output = np.zeros([self.data_wrapper.k + 1])
        for i in range(len(x)):
            xi = x[i]
            y_output[i] = func(xi)
        return y_output

    def _gen_y_approximant_function(self, x, result: Result):
        y_output = np.zeros([self.data_wrapper.k + 1])
        for i in range(len(x)):
            xi = x[i]
            y_output[i] = self.approximant_function(
                xi, result.a, result.b, result.c, result.d)
        return y_output

    def plot(self, nelder_mead: Result, lm: Result, sa: Result, de: Result):
        x = self.data_wrapper.data[:, 0]
        assert len(x) == self.data_wrapper.k + 1

        fig_data, ax_data = plt.subplots()
        fig_data: Figure
        ax_data: Axes

        fig_results, ax_results = plt.subplots()
        fig_results: Figure
        ax_results: Axes

        # 'noisy' data points
        y_noisy = self.data_wrapper.data[:, 1]

        y_clean = self._gen_y_clean(x, self.data_wrapper.f)
        y_nelder_mead = self._gen_y_approximant_function(x, nelder_mead)
        y_lm = self._gen_y_approximant_function(x, lm)
        y_sa = self._gen_y_approximant_function(x, sa)
        y_de = self._gen_y_approximant_function(x, de)

        # ------------------------------------------------------------
        # Plot initial data
        # ------------------------------------------------------------
        ax_data.set_ylim([-150, 150])
        ax_data.plot(x, y_noisy, '.', alpha=0.2, label="Data with noise $(x_k, y_k)$")
        ax_data.plot(x, y_clean, '-', alpha=0.8, label="$f(x)$")

        ax_data.set_xlabel('x')
        ax_data.set_ylabel('y')
        ax_data.legend()

        fig_data.savefig('data/initial-data.pdf')

        # ------------------------------------------------------------
        # Plot all results
        # ------------------------------------------------------------
        ax_results.set_ylim([-150, 150])
        ax_results.plot(x, y_noisy, '.', alpha=0.1, label="Data with noise $(x_k, y_k)$")
        # ax_results.plot(x, y_clean, '-', alpha=0.1, label="$f(x)$")
        ax_results.plot(x, y_nelder_mead, '-', alpha=1, label="Approx: Nelder-mead")
        ax_results.plot(x, y_lm, '-', alpha=1, label="Approx: L-M")
        ax_results.plot(x, y_sa, '-', alpha=1, label="Approx: S-A")
        ax_results.plot(x, y_de, '-', alpha=1, label="Approx: DE")
        ax_results.set_xlabel('x')
        ax_results.set_ylabel('y')
        ax_results.legend()
        fig_results.savefig('data/all-results.pdf')

        # ------------------------------------------------------------
        # Plot individual
        # ------------------------------------------------------------
        for y_data, plot_name, filename in [
            [y_nelder_mead, "Approx: Nelder-mead", "single-nm.pdf"],
            [y_lm, "Approx: L-M", "single-lm.pdf"],
            [y_sa, "Approx: S-A", "single-sa.pdf"],
            [y_de, "Approx: DE", "single-de.pdf"],
        ]:
            fig_single, ax_single = plt.subplots()
            fig_single: Figure
            ax_single: Axes
            ax_single.set_ylim([-150, 150])
            ax_single.plot(x, y_noisy, '-', alpha=0.5, label="Data with noise $(x_k, y_k)$")
            ax_single.plot(x, y_data, '-', alpha=1, linewidth=0.5, label=plot_name)
            ax_single.set_xlabel('x')
            ax_single.set_ylabel('y')
            ax_single.legend()
            fig_single.savefig(f'data/{filename}')

    # --------------------------------------------------
    # Levenberg-Marquardt algorithm
    # --------------------------------------------------

    def _fun_residual(self, abcd):
        a, b, c, d = abcd
        assert self.data_wrapper is not None
        residuals = list()
        for k in range(0, self.data_wrapper.k + 1):
            x = self.data_wrapper.data[k][0]
            y = self.data_wrapper.data[k][1]
            evaluated_value = self.approximant_function(x, a, b, c, d)
            residual = evaluated_value - y
            residuals.append(residual)
        return residuals

    def levenberg_marquardt_algorithm(self, epsilon, initial_guess=None):
        # return Result(a=0, b=0, c=0, d=0, iter_count=0, eval_count=0)
        logging.info("---------- Starting levenberg_marquardt_algorithm() ----------")
        sys.stdout.flush()
        initial_guess = initial_guess or [0, 0, 0, 0]
        res: optimize.OptimizeResult = optimize.least_squares(
            self._fun_residual,
            initial_guess,
            method='lm',
            xtol=epsilon,
            verbose=2,
        )
        logging.info("Finished levenberg_marquardt_algorithm(): res=%s", res)
        assert res.success, f"levenberg_marquardt_algorithm() was not successfull"
        return Result(
            a=res.x[0],
            b=res.x[1],
            c=res.x[2],
            d=res.x[3],
            eval_count=self.eval_count,
            iter_count=-1
        )

    def nelder_mead_approximation(self, epsilon, initial_guess=None):
        # return Result(a=0, b=0, c=0, d=0, iter_count=0, eval_count=0)
        logging.info("---------- Starting nelder_mead_approximation() ----------")
        sys.stdout.flush()
        self.eval_count = 0
        initial_guess = initial_guess or [0, 0, 0, 0]
        options = dict(
            xatol=epsilon,
        )
        res: optimize.OptimizeResult = optimize.minimize(
            self.means_of_least_squares,
            initial_guess,
            method="Nelder-Mead",
            options=options,
        )
        assert res.success, f"nelder_mead_approximation() was not successfull"
        return Result(
            a=res.x[0],
            b=res.x[1],
            c=res.x[2],
            d=res.x[3],
            eval_count=self.eval_count,
            iter_count=res.nit,
        )

    def simulated_annealing(self, epsilon, initial_guess=None):
        seed = 2340788695
        logging.info("---------- Starting simulated_annealing(seed=%s) ----------", seed)
        sys.stdout.flush()
        self.eval_count = 0
        initial_guess = initial_guess or [0, 0, 0, 0]
        res: optimize.OptimizeResult = optimize.basinhopping(
            self.means_of_least_squares,
            initial_guess,
            seed=seed,
            niter=10,
        )
        return Result(
            a=res.x[0],
            b=res.x[1],
            c=res.x[2],
            d=res.x[3],
            eval_count=self.eval_count,
            iter_count=res.nit,
        )

    def differential_evolution(self, epsilon):
        seed = 1150535069
        logging.info("---------- Starting differential_evolution(seed=%s) ----------", seed)
        sys.stdout.flush()
        self.eval_count = 0
        bounds = [(-5.0, 5.0), (-5.0, 5.0), (-5.0, 5.0), (-5.0, 5.0)]

        res: optimize.OptimizeResult = optimize.differential_evolution(
            self.means_of_least_squares,
            bounds=bounds,
            seed=seed,
            maxiter=1000,
        )
        logging.debug("differential_evolution() -> %s", res)
        return Result(
            a=res.x[0],
            b=res.x[1],
            c=res.x[2],
            d=res.x[3],
            eval_count=self.eval_count,
            iter_count=res.nit,
        )


class Main:
    def __init__(self, epsilon=None, seed=None):
        logging.info("seed=%s", seed)
        self.epsilon = epsilon
        self.seed = seed
        self.data_wrapper = DataWrapper(seed=seed).prepare_data()
        self.task = Task(self.data_wrapper)

    def main(self):
        EPSILON = self.epsilon or 0.001
        initial_guess = [3.0, 3.0, 3.0, 3.0]

        result_nelder_mead = self.task.nelder_mead_approximation(EPSILON, initial_guess)
        result_lm = self.task.levenberg_marquardt_algorithm(EPSILON, initial_guess)
        result_sa = self.task.simulated_annealing(EPSILON, initial_guess)
        result_de = self.task.differential_evolution(EPSILON)

        def dump(filename, method_name, result: Result):
            f_value = self.task.means_of_least_squares(
                [result.a, result.b, result.c, result.d]
            )
            iter_count = result.iter_count if result.iter_count > 0 else '-'
            contents = f"methodname,a,b,c,d,eval_count,iter_count,f_value\n" \
                       f"\"{method_name}\"," \
                       f"{result.a},{result.b},{result.c},{result.d}," \
                       f"{result.eval_count},{iter_count}," \
                       f"{f_value}\n"
            pathlib.Path(f'data/{filename}.csv').write_text(contents)

        logging.info("result_nelder_mead: %s", result_nelder_mead)
        logging.info("result_lm: %s", result_lm)
        logging.info("result_sa: %s", result_sa)
        logging.info("result_de: %s", result_de)

        dump('nm', "Nelder-Mead", result_nelder_mead)
        dump('lm', "Levenberg-Marquardt", result_lm)
        dump('sa', "Simulated Annealing", result_sa)
        dump('de', "Differential Evolution", result_de)

    def plot(self):
        logging.info("---------- PLOT ----------")
        sys.stdout.flush()

        def load(name) -> typing.Tuple[str, Result]:
            contents = pathlib.Path(f'data/{name}.csv').read_text()
            method_name, a, b, c, d, eval_count, iter_count, f_value = contents.splitlines()[1].split(',')
            return method_name, Result(
                a=float(a),
                b=float(b),
                c=float(c),
                d=float(d),
                iter_count=int(iter_count) if iter_count != '-' else '-1',
                eval_count=int(eval_count) if eval_count != '-' else '-1',
            )

        _, result_nelder_mead = load('nm')
        _, result_lm = load('lm')
        _, result_sa = load('sa')
        _, result_de = load('de')

        logging.info("result_nelder_mead: %s", result_nelder_mead)
        logging.info("result_lm: %s", result_lm)
        logging.info("result_sa: %s", result_sa)
        logging.info("result_de: %s", result_de)

        self.task.plot(
            result_nelder_mead,
            result_lm,
            result_sa,
            result_de
        )


def setup_logging(level=logging.DEBUG):
    logging.basicConfig(level=level, stream=sys.stdout)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)


if __name__ == '__main__':
    setup_logging(level=logging.INFO)
    main = Main(seed=6832299506)
    main.main()
    main.plot()
    logging.info("Finished OK")
